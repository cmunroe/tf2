# Team Fortress 2


A better option instead of downloading a ~7GB docker image is to use the thin version at: https://gitlab.com/cmunroe/tf2-thin

This is a TF2 docker image based upon Debian:Buster and https://gitlab.com/cmunroe/steamcmd.

## Other Projects / References

https://github.com/spiretf/docker-comp-server

https://github.com/spiretf/docker-tf2-server

https://github.com/CM2Walki/TF2

https://github.com/GameServers/TeamFortress2

https://hub.docker.com/r/cm2network/tf2

https://hub.docker.com/r/gameservers/teamfortress2

